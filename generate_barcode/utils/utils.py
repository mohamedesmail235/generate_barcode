# Copyright (c) 2021, Frappe Technologies Pvt. Ltd. and Contributors
# License: GNU General Public License v3. See license.txt
import frappe
import jwt
from frappe import _
from stdnum import ean
from frappe.utils import flt,getdate,cint

def add_barcode(doc,method=None):
    doc.append("barcodes", {"barcode": str(doc.name).replace("-","")})

@frappe.whitelist()
def get_invoices_for_mall(date=getdate()):
    invoices = frappe.get_all("Sales Invoice", filters={"posting_date": ["=",date],"cost_center":"Alamein Branch - MCR","docstatus":1}, fields=["name","posting_date","posting_time","total","rounded_total","net_total","grand_total","total_taxes_and_charges","is_return","return_against"])
    if invoices:
        return invoices
    else:
        return {}
