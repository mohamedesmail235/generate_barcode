from setuptools import setup, find_packages

with open("requirements.txt") as f:
	install_requires = f.read().strip().split("\n")

# get version from __version__ variable in generate_barcode/__init__.py
from generate_barcode import __version__ as version

setup(
	name="generate_barcode",
	version=version,
	description="Generate-Barcode",
	author="MiM",
	author_email="m.ismail@datavaluenet.net",
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
